package com.mygdx.game.Game.api;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public class BaseGame extends Game {
    public Skin skin;
    public FileHandle rankFile;

    @Override
    public void create() {
        skin = new Skin();
        rankFile = new FileHandle(Gdx.files.getLocalStoragePath() + "Scores.txt");
        rankFile.writeString(" ",true);
    }

    public void dispose() {
        skin.dispose();
    }
}
