package com.mygdx.game.Game.impl;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.mygdx.game.Game.api.BaseGame;
import com.mygdx.game.Screen.impl.PunchMenu;

public class PunchGame extends BaseGame {

    public void create() {
        super.create();
        // initialize resources common to multiple screens

        // Background
        Texture backTex = new Texture("background.jpg");
        backTex.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        skin.add("backTex", backTex);

        // Bitmap font
        BitmapFont uiFont = new BitmapFont(Gdx.files.internal("cooper.fnt"));
        uiFont.getRegion().getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

        skin.add("uiFont", uiFont);

        // Label style
        Label.LabelStyle uiLabelStyle = new Label.LabelStyle(uiFont, Color.BLUE);
        skin.add("uiButtonLabelStyle", uiLabelStyle);

        BitmapFont font = new BitmapFont();
        Label.LabelStyle style = new Label.LabelStyle(font, Color.GREEN);

        skin.add("uiLabelStyle", style);

        // TextButton style
        TextButton.TextButtonStyle uiTextButtonStyle = new TextButton.TextButtonStyle();

        uiTextButtonStyle.font = uiFont;
        uiTextButtonStyle.fontColor = Color.NAVY;

        Texture upTex = new Texture(Gdx.files.internal("button-1.png"));
        skin.add("buttonUp", new NinePatch(upTex, 26, 26, 16, 20));
        uiTextButtonStyle.up = skin.getDrawable("buttonUp");

        Texture overTex = new Texture(Gdx.files.internal("button-2.png"));
        skin.add("buttonOver", new NinePatch(overTex, 26, 26, 16, 20));
        uiTextButtonStyle.over = skin.getDrawable("buttonOver");
        uiTextButtonStyle.overFontColor = Color.BLUE;

        Texture downTex = new Texture(Gdx.files.internal("button-3.png"));
        skin.add("buttonDown", new NinePatch(downTex, 26, 26, 16, 20));
        uiTextButtonStyle.down = skin.getDrawable("buttonDown");
        uiTextButtonStyle.downFontColor = Color.BLUE;

        skin.add("uiTextButtonStyle", uiTextButtonStyle);

        Texture backTexture = new Texture("pause.png");
        skin.add("pauseImage", backTexture);

        PunchMenu punchMenu = new PunchMenu(this);
        setScreen(punchMenu);
    }

    public void initializeWormSkins(){

    }
}