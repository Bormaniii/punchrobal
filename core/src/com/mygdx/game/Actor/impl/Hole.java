package com.mygdx.game.Actor.impl;

import com.mygdx.game.Actor.api.BaseActor;

public class Hole extends BaseActor {
    private boolean isCreated;
    private boolean isEnabled;
    private Worm worm;

    public Hole() {
        super();
    }

    public boolean isCreated() {
        return isCreated;
    }

    public void setCreated(boolean created) {
        isCreated = created;
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
    }

    public Worm getWorm() {
        return worm;
    }

    public void setWorm(Worm worm) {
        this.worm = worm;
    }
}