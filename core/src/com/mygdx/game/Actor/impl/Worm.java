package com.mygdx.game.Actor.impl;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.mygdx.game.Actor.api.AnimatedActor;

public class Worm extends AnimatedActor {
    public int wormType;
    public String[][] wormTypeAnimations;
    public int punched;
    private int punchLimit;
    private float standDuration;
    public InputListener inputListener;

    public Worm() {
        wormTypeAnimations = new String[2][4];
        wormTypeAnimations[0][0] = "Worm1/arrive";
        wormTypeAnimations[0][1] = "Worm1/crush";
        wormTypeAnimations[0][2] = "Worm1/stand";
        wormTypeAnimations[1][0] = "Worm2/arrive";
        wormTypeAnimations[1][1] = "Worm2/firstHit";
        wormTypeAnimations[1][2] = "Worm2/stand";
        wormTypeAnimations[1][3] = "Worm2/boom";

        setWormTypeAnimations();

        inputListener = new InputListener();
    }

    public void act(float dt) {
        super.act(dt);
    }

    public void setWormType(int type) {
        wormType = type;
        switch (wormType) {
            case 0:
                punched = 0;
                punchLimit = 1;
                standDuration = 0.7f;
                break;
            case 1:
                punched = 0;
                punchLimit = 2;
                standDuration = 0.8f;
                break;
        }
    }

    private void setWormTypeAnimations() {
        for (int i = 0; i < wormTypeAnimations.length; i++) {
            Animation<TextureRegion> animGrow = this.setAnimation(wormTypeAnimations[i][0], 3,
                    Animation.PlayMode.NORMAL, 0.06f);
            this.storeAnimation("grow" + i, animGrow);

            Animation<TextureRegion> animEscape = this.setAnimation(wormTypeAnimations[i][0], 3,
                    Animation.PlayMode.REVERSED, 0.06f);
            this.storeAnimation("escape" + i, animEscape);

            Animation<TextureRegion> animStand = this.setAnimation(wormTypeAnimations[i][2], 1,
                    Animation.PlayMode.LOOP, standDuration);
            this.storeAnimation("stand" + i, animStand);
            Animation<TextureRegion> animCrush;
            if (i == 0) {
                animCrush = this.setAnimation(wormTypeAnimations[i][1], 5,
                        Animation.PlayMode.NORMAL, 0.05f);
                this.storeAnimation("crush" + i, animCrush);
            } else if (i == 1) {
                animCrush = this.setAnimation(wormTypeAnimations[i][1], 2,
                        Animation.PlayMode.NORMAL, 0.06f);
                this.storeAnimation("crush" + i, animCrush);

                Animation<TextureRegion> animBoom = this.setAnimation(wormTypeAnimations[i][3], 3,
                        Animation.PlayMode.NORMAL, 0.05f);
                this.storeAnimation("boom" + i, animBoom);
            }
        }
    }

    public float getStandDuration() {
        return standDuration;
    }

    public void setStandDuration(float standDuration) {
        this.standDuration = standDuration;
    }

    public int getPunchLimit() {
        return punchLimit;
    }

    public void setPunchLimit(int punchLimit) {
        this.punchLimit = punchLimit;
    }
}