package com.mygdx.game.Actor.api;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class BaseActor extends Actor {
    public TextureRegion region;
    public Polygon boundingPolygon;
//    public float velocityX;
//    public float velocityY;

    public BaseActor() {
        super();
        region = new TextureRegion();
        boundingPolygon = null;
//        velocityX = 0;
//        velocityY = 0;
    }

    public void setTexture(Texture t) {
        int w = t.getWidth();
        int h = t.getHeight();
        setWidth(w);
        setHeight(h);
        region.setRegion(t);
    }

    public void setRectangleBoundary() {
        float width = getWidth();
        float height = getHeight();
        float[] verticles = {0, 0, width, 0, width, height, 0, height};
        boundingPolygon = new Polygon(verticles);
        boundingPolygon.setOrigin(getOriginX(), getOriginY());
    }

    public void setEllipseBoundary() {
        int n = 12; //number of vertices
        float width = getWidth();
        float height = getHeight();
        float[] verticles = new float[2 * n];
        for (int i = 0; i < n; i++) {
            float t = i * 6.28f / n;  //For get ellipse shape
            // x-coordinate
            verticles[2 * i] = width / 2 * MathUtils.cos(t) + width / 2;
            // y-coordinate
            verticles[2 * i + 1] = height / 2 * MathUtils.cos(t) + height / 2;
        }
        boundingPolygon = new Polygon(verticles);
        boundingPolygon.setOrigin(getOriginX(), getOriginY());
    }

    public Polygon getBoundingPolygon() {
        boundingPolygon.setPosition(getX(), getY());
        boundingPolygon.setRotation(getRotation());
        return boundingPolygon;
    }

    //    public Rectangle getBoundingRectangle() {
//        boundary.set(getX(), getY(), getWidth(), getHeight());
//        return boundary;
//    }

    public boolean overlaps(BaseActor other)
    {
        Polygon poly1 = this.getBoundingPolygon();
        Polygon poly2 = other.getBoundingPolygon();

        if (!poly1.getBoundingRectangle().overlaps(poly2.getBoundingRectangle())){
            return false;
        }else {
            return true;
        }

    }

    public void act(float dt) {
        super.act(dt);
//        moveBy(velocityX * dt, velocityY * dt);
    }

    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha); // but.... this is empty, so can be deleted here...
        Color c = getColor();
        batch.setColor(c.r, c.g, c.b, c.a);
        if (isVisible())
            batch.draw(region, getX(), getY(), getOriginX(), getOriginY(),
                    getWidth(), getHeight(), getScaleX(), getScaleY(), getRotation());
    }

    public void copy(BaseActor original) {
        this.region = new TextureRegion(original.region);
        if (original.boundingPolygon != null) {
            this.boundingPolygon = new Polygon(original.boundingPolygon.getVertices());
        }
        this.setPosition(original.getX(), original.getY());
        this.setOriginX(original.getOriginX());
        this.setOriginY(original.getOriginY());
        this.setWidth(original.getWidth());
        this.setHeight(original.getHeight());
        this.setColor(original.getColor());
        this.setVisible(original.isVisible());
    }

    public BaseActor clone() {
        BaseActor newbie = new BaseActor();
        newbie.copy(this);
        return newbie;
    }
}
