package com.mygdx.game.Actor.api;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;

import java.util.HashMap;

public class AnimatedActor extends BaseActor {

    private Animation<TextureRegion> activeAnim;
    private String activeName;
    private HashMap<String, Animation<TextureRegion>> animationStorage;
    private float elapsedTime;

    public AnimatedActor() {
        super();
        this.activeAnim = null;
        this.activeName = null;
        this.animationStorage = new HashMap<>();
        this.elapsedTime = 0;
    }

    public void clearAnimations(){
        animationStorage.clear();
    }

    public void storeAnimation(String name, Animation anim) {
        animationStorage.put(name, anim);
        if (activeName == null) {
            setActiveAnimation(name);
        }
    }

    public void storeAnimation(String name, Texture t) {
        TextureRegion reg = new TextureRegion(t);
        TextureRegion[] frames = {reg};
        Animation anim = new Animation(1.0f, frames);
        storeAnimation(name, anim);
    }

    public void setActiveAnimation(String name) {
        if (!animationStorage.containsKey(name)) {
            System.out.println("There is no animation named " + name);
            return;
        }

        activeName = name;
        activeAnim = animationStorage.get(name);
        elapsedTime = 0;

        Texture t = activeAnim.getKeyFrame(0).getTexture();
        setWidth(t.getWidth());
        setHeight(t.getHeight());
    }

    public Animation<TextureRegion> setAnimation(String name, int numberOfFrames, Animation.PlayMode mode, float duration) {
        TextureRegion[] frames = new TextureRegion[numberOfFrames];
        for (int i = 1; i <= numberOfFrames; i++) {
            String fileName = name + i + ".png";
            Texture t = new Texture(fileName);
            t.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
            frames[i - 1] = new TextureRegion(t);
        }
        Array<TextureRegion> framesArray = new Array<>(frames);

        return new Animation(duration, framesArray, mode);
    }

    public void act(float dt) {
        super.act(dt);
        elapsedTime += dt;
    }

    public void draw(Batch batch, float parentAlpha) {
        region.setRegion(activeAnim.getKeyFrame(elapsedTime));
        super.draw(batch, parentAlpha);
    }

    public Animation<TextureRegion> getActiveAnim() {
        return activeAnim;
    }

    public void setActiveAnim(Animation<TextureRegion> activeAnim) {
        this.activeAnim = activeAnim;
    }

    public String getActiveName() {
        return activeName;
    }

    public void setActiveName(String activeName) {
        this.activeName = activeName;
    }

    public HashMap<String, Animation<TextureRegion>> getAnimationStorage() {
        return animationStorage;
    }

    public void setAnimationStorage(HashMap<String, Animation<TextureRegion>> animationStorage) {
        this.animationStorage = animationStorage;
    }

    public float getElapsedTime() {
        return elapsedTime;
    }

    public void setElapsedTime(float elapsedTime) {
        this.elapsedTime = elapsedTime;
    }
}
