package com.mygdx.game.Screen.api;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.mygdx.game.Game.api.BaseGame;

public abstract class BaseScreen implements Screen, InputProcessor {
    protected BaseGame game;

    public Stage mainStage;
    protected Stage uiStage;

    protected Table mainTable;
    protected Table uiTable;

    protected final int viewWidth = Gdx.graphics.getWidth();
    protected final int viewHeight = Gdx.graphics.getHeight();

    private boolean paused;

    public BaseScreen(BaseGame g) {
        game = g;

        mainStage = new Stage(new FitViewport(viewWidth, viewHeight));
        uiStage = new Stage(new FitViewport(viewWidth, viewHeight));

        uiTable = new Table();
        uiTable.setFillParent(true);

        mainTable = new Table();
        mainTable.setFillParent(true);

        paused = false;

        InputMultiplexer im = new InputMultiplexer(this, mainStage, uiStage);
        Gdx.input.setInputProcessor(im);

        create();
    }

    public abstract void create();

    public abstract void update(float dt);

    @Override
    public void show() {

    }

    public void render(float dt) {
        uiStage.act(dt);

        //Pause gameplay only, not menu
        if (!isPaused()) {
            mainStage.act();
        }
        update(dt);

        // render
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        mainStage.draw();
        uiStage.draw();
    }

    public boolean isPaused() {
        return paused;
    }

    public void setPaused(boolean p) {
        this.paused = p;
    }

    public void togglePaused() {
        paused = !paused;
    }

    public void resize(int width, int height) {
        mainStage.getViewport().update(width, height, true);
        uiStage.getViewport().update(width, height, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    public Stage getMainStage() {
        return mainStage;
    }

    public void setMainStage(Stage mainStage) {
        this.mainStage = mainStage;
    }

    public Stage getUiStage() {
        return uiStage;
    }

    public void setUiStage(Stage uiStage) {
        this.uiStage = uiStage;
    }

    public Table getMainTable() {
        return mainTable;
    }

    public void setMainTable(Table mainTable) {
        this.mainTable = mainTable;
    }

    public Table getUiTable() {
        return uiTable;
    }

    public void setUiTable(Table uiTable) {
        this.uiTable = uiTable;
    }

    public int getViewWidth() {
        return viewWidth;
    }

    public int getViewHeight() {
        return viewHeight;
    }
}
