package com.mygdx.game.Screen.impl;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.mygdx.game.Game.api.BaseGame;
import com.mygdx.game.Screen.Service.RankingService;
import com.mygdx.game.Screen.api.BaseScreen;

public class PunchRanking extends BaseScreen {
    private RankingService rankingService;

    private Button pauseButton;
    private Label scoreLabel;

    public PunchRanking(BaseGame g) {
        super(g);
    }

    public void create() {
        rankingService = new RankingService(game.rankFile);

        mainTable.background(game.skin.getDrawable("backTex"));

        Texture titleTex = new Texture("rankingText.png");
        titleTex.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        Image titleImage = new Image(titleTex);

        Button.ButtonStyle pauseStyle = new Button.ButtonStyle();
        pauseStyle.up = game.skin.getDrawable("pauseImage");

        pauseButton = new Button(pauseStyle);

        pauseButton.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                game.setScreen(new PunchMenu(game));
                return true;
            }
        });


        uiTable.add().expandX();
        uiTable.add(titleImage).padTop(80);
        uiTable.add(pauseButton).right().padTop(20).padRight(20);
        uiTable.row();
        uiTable.add().expandY();
        uiTable.row();
        String[] scores = rankingService.readStringFile();
        for (int i = 1; i <= scores.length; i++) {
            String textLabel = i + ". " + scores[i - 1];
            scoreLabel = new Label(textLabel, game.skin, "uiLabelStyle");
            scoreLabel.setFontScale(4);

            uiTable.add().expandX();
            uiTable.add(scoreLabel);
            uiTable.add().expandX();
            uiTable.row();
        }
        uiTable.row();
        uiTable.add().expandY();

        mainStage.addActor(mainTable);
        uiStage.addActor(uiTable);

    }

    public void update(float dt) {

    }
}
