package com.mygdx.game.Screen.impl;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.mygdx.game.Actor.api.AnimatedActor;
import com.mygdx.game.Actor.api.BaseActor;
import com.mygdx.game.Actor.impl.Hole;
import com.mygdx.game.Game.api.BaseGame;
import com.mygdx.game.Screen.Service.PunchLevelService;
import com.mygdx.game.Screen.Service.RankingService;
import com.mygdx.game.Screen.api.BaseScreen;

import java.util.ArrayList;

public class PunchLevel extends BaseScreen {
    private PunchLevelService punchLevelService;
    private RankingService rankingService;

    public float spawnTimer;
    public float spawnInterval;
    public float spawnIntervalMin;
    public float spawnIntervalMax;
    public int spawnSeparator;

    public int lives;
    private int livesSeparator;
    private int livesSeparatorUpdater;
    public int score;
    public int escaped;
    private boolean gameOver;

    public float volume;
    public Sound beep;

    private BaseActor background;

    public ArrayList<Hole> holes;
    public int holesCreated;
    public int holesLimit;
    private int holeSeparator;

    private BaseActor gameOverImage;

    private AnimatedActor pauseCount;
    private float pauseTimer;

    private Button pauseButton;
    private Label livesLabelText;
    private String livesText;
    private Label livesLabelNumber;
    private Label scoreLabelText;
    private String scoreText;
    private Label scoreLabelNumber;
    private Label escapedLabelText;
    private String escapeText;
    private Label escapedLabelNumber;

    public PunchLevel(BaseGame g) {
        super(g);
    }

    public void create() {
        punchLevelService = new PunchLevelService(this);
        rankingService = new RankingService(game.rankFile);

        spawnTimer = 0;
        spawnInterval = 0.8f;
        spawnIntervalMin = 0.5f;
        spawnIntervalMax = 1f;
        spawnSeparator = 10;

        lives = 5;
        livesSeparator = 30;
        livesSeparatorUpdater = 30;
        score = 0;
        escaped = 0;
        gameOver = false;

        volume = 0.8f;
        beep = Gdx.audio.newSound(Gdx.files.internal("crushSound.mp3"));

        background = new BaseActor();
        background.setTexture(game.skin.get("backTex", Texture.class));
        background.setHeight(viewHeight);
        background.setWidth(viewWidth);
        background.setPosition(0,0);
        background.setVisible(true);
        mainStage.addActor(background);

        // Add every new hole to the table
        holes = new ArrayList<>();
        holesCreated = 0;
        holesLimit = 18;
        holeSeparator = 25;

        punchLevelService.setHoles();
        punchLevelService.setWorms();

        ButtonStyle pauseStyle = new ButtonStyle();
        pauseStyle.up = game.skin.getDrawable("pauseImage");

        pauseButton = new Button(pauseStyle);
        pauseButton.setPosition(viewWidth-pauseButton.getWidth()-30,viewHeight-pauseButton.getHeight()-30);
        uiStage.addActor(pauseButton);

        pauseButton.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (lives > 0) {
                    if (isPaused()) {
                        pauseCount.setVisible(isPaused());
                        pauseCount.setActiveAnimation("count");
                        pauseTimer = 2.4f;
                    } else {
                        togglePaused();
                    }
                } else {
                    game.setScreen(new PunchMenu(game));
                }
                return true;
            }
        });

        scoreText = "Score: ";
        scoreLabelText = new Label(scoreText, game.skin, "uiLabelStyle");
        scoreLabelText.setFontScale(4);

        scoreLabelNumber = new Label(Integer.toString(score), game.skin, "uiLabelStyle");
        scoreLabelNumber.setFontScale(4);

        escapeText = "Worms escaped: ";
        escapedLabelText = new Label(escapeText, game.skin, "uiLabelStyle");
        escapedLabelText.setFontScale(4);

        escapedLabelNumber = new Label(Integer.toString(escaped), game.skin, "uiLabelStyle");
        escapedLabelNumber.setFontScale(4);

        livesText = "Lives: ";
        livesLabelText = new Label(livesText, game.skin, "uiLabelStyle");
        livesLabelText.setFontScale(4);

        livesLabelNumber = new Label(Integer.toString(lives), game.skin, "uiLabelStyle");
        livesLabelNumber.setFontScale(4);

        uiTable.add(scoreLabelText).left().pad(20, 20, 0, 0);
        uiTable.add(scoreLabelNumber).pad(20, 20, 0, 0);
        uiTable.add().expandX();
        uiTable.row();
        uiTable.add(escapedLabelText).left().padLeft(20);
        uiTable.add(escapedLabelNumber).padLeft(20);
        uiTable.add().expandX();
        uiTable.row();
        uiTable.add(livesLabelText).left().padLeft(20);
        uiTable.add(livesLabelNumber).padLeft(20);
        uiTable.add().expandX();
        uiTable.row();
        uiTable.add().expandY();
        uiStage.addActor(uiTable);

        pauseCount = new AnimatedActor();
        Animation<TextureRegion> anim = pauseCount.setAnimation("pauseCount-", 3, Animation.PlayMode.REVERSED, 0.8f);
        pauseCount.storeAnimation("count", anim);
        pauseCount.setVisible(false);
        uiStage.addActor(pauseCount);

        pauseTimer = 0;

        gameOverImage = new BaseActor();
        gameOverImage.setTexture(new Texture("gameOverImage.png"));
        gameOverImage.setPosition(viewWidth / 2 - gameOverImage.getWidth() / 2, viewHeight / 2);
        gameOverImage.setVisible(false);
        uiStage.addActor(gameOverImage);
    }

    public void update(float dt) {
        System.out.println(Gdx.app.getJavaHeap()+" u");
        System.out.println(Gdx.app.getNativeHeap()+" u");
        if (!isPaused()) {
            if (lives > 0) {
                //Level up and gain lives
                if (score > spawnSeparator && spawnIntervalMin > 0.2f && spawnIntervalMax > 0.4f) {
                    spawnIntervalMin -= 0.03f;
                    spawnIntervalMax -= 0.06f;
                    spawnSeparator += 8;
                }
                if (score > livesSeparator) {
                    lives++;
                    livesSeparator += livesSeparatorUpdater;
                    livesSeparatorUpdater += 30;
                }

                //reset SpawnTimer
                spawnTimer += dt;
                spawnInterval = MathUtils.random(spawnIntervalMin, spawnIntervalMax);

                //Spawn new worms
                if (spawnTimer > spawnInterval) {
                    int wormQty = MathUtils.random(1, 4);
                    switch (wormQty) {
                        case 1:
                        case 2:
                        case 3:
                            punchLevelService.generateWorm();
                            break;
                        case 4:
                            punchLevelService.generateWorm();
                            punchLevelService.generateWorm();
                            break;
                    }
                }

                if (score > holeSeparator && holesCreated<holesLimit) {
                    punchLevelService.unlockHole();
                    holeSeparator += holeSeparator;
                }


                //Check whether a worm should change animation or disappear
                punchLevelService.changeWormAnimations();

                //Update labels
                scoreLabelNumber.setText(score);
                escapedLabelNumber.setText(escaped);
                livesLabelNumber.setText(lives);
            } else {
                lives = 0;
                gameOverImage.setVisible(true);

                if (!gameOver) {
                    ArrayList<Integer> newScores = rankingService.getSortedScores(score);
                    rankingService.clearFile();

                    int limit = newScores.size() > 10 ? 10 : newScores.size();
                    //Add only 10 best scores
                    for (int i = 0; i < limit; i++) {
                        rankingService.writeNewScore(newScores.get(i).toString());
                    }
                    gameOver = true;
                }
            }
        } else {
            if (pauseTimer > 0) {
                pauseTimer -= dt;
            }
            if (pauseTimer < 0) {
                pauseCount.setVisible(false);
                togglePaused();
                pauseTimer = 0;
            }
        }
    }

    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if(button == Input.Buttons.BACK){
            if (lives > 0) {
                if (isPaused()) {
                    pauseCount.setVisible(isPaused());
                    pauseCount.setActiveAnimation("count");
                    pauseTimer = 2.4f;
                } else {
                    togglePaused();
                }
            } else {
                game.setScreen(new PunchMenu(game));
            }
            return true;
        }
        return false;
    }

    public void dispose() {
        beep.dispose();
        mainStage.dispose();
        uiStage.dispose();
    }
}