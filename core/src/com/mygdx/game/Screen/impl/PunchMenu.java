package com.mygdx.game.Screen.impl;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.mygdx.game.Game.api.BaseGame;
import com.mygdx.game.Screen.api.BaseScreen;

public class PunchMenu extends BaseScreen {

    public PunchMenu(BaseGame g) {
        super(g);
    }

    public void create() {
        mainTable.background(game.skin.getDrawable("backTex"));

        Texture titleTex = new Texture("logoText.png");
        titleTex.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        Image titleImage = new Image(titleTex);

        TextButton startButton = new TextButton("Start", game.skin, "uiTextButtonStyle");
        startButton.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true; //Continue processing
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                game.setScreen(new PunchLevel(game));
            }
        });

        TextButton rankingButton = new TextButton("Ranking", game.skin, "uiTextButtonStyle");
        rankingButton.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true; //Continue processing
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                game.setScreen(new PunchRanking(game));
            }
        });

        TextButton quitButton = new TextButton("Quit game", game.skin, "uiTextButtonStyle");
        quitButton.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true; //Continue processing
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                dispose();
                Gdx.app.exit();
                System.exit(-1);
            }
        });

        float w = quitButton.getWidth();

        uiTable.add(titleImage).padTop(80);
        uiTable.row();
        uiTable.add().expandY();
        uiTable.row();
        uiTable.add(startButton).width(w);
        uiTable.row();
        uiTable.add(rankingButton).width(w);
        uiTable.row();
        uiTable.add(quitButton);
        uiTable.row();
        uiTable.add().expandY();

        mainStage.addActor(mainTable);
        uiStage.addActor(uiTable);
    }

    public void update(float dt) {
    }
}