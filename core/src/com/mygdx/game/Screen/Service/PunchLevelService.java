package com.mygdx.game.Screen.Service;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.mygdx.game.Actor.impl.Hole;
import com.mygdx.game.Actor.impl.Worm;
import com.mygdx.game.Screen.impl.PunchLevel;

public class PunchLevelService {
    private PunchLevel punchLevel;

    public PunchLevelService(PunchLevel punchLevel) {
        this.punchLevel = punchLevel;
    }

    public void setHoles() {
        for (int i = 0; i < punchLevel.holesLimit; i++) {
            boolean freeSpace = false;
            Hole hole = new Hole();
            hole.setTexture(new Texture("hole.png"));
            hole.setOrigin(0, 0);
            while (!freeSpace) {
                float randHoleX = MathUtils.random(0, punchLevel.getViewWidth() - hole.getWidth());
                float randHoleY = MathUtils.random(0+(punchLevel.getViewHeight())/12, punchLevel.getViewHeight()-hole.getHeight()-(punchLevel.getViewHeight())/7);

                hole.setPosition(randHoleX, randHoleY);
                hole.setVisible(false);
                hole.setEnabled(false);
                hole.setCreated(false);
                hole.setEllipseBoundary();

                freeSpace = true;
                for (Hole existingHole : punchLevel.holes) {
                    if (hole.overlaps(existingHole)) {
                        freeSpace = false;
                        break;
                    }
                }
            }
            punchLevel.holes.add(hole);
            punchLevel.mainStage.addActor(hole);
        }
        while (punchLevel.holesCreated < punchLevel.holesLimit / 2) {
            unlockHole();
        }
    }

    public void setWorms(){
        for(Hole hole : punchLevel.holes){
            Worm worm = new Worm();
            worm.setWormType(0);
            worm.setOrigin(worm.getWidth() / 2, worm.getHeight() / 2);
            worm.setEllipseBoundary();
            //set the worm in the hole
            worm.setPosition(hole.getX() + (hole.getWidth() / 2) - worm.getWidth() / 2,
                    hole.getY() + (hole.getHeight() / 2) - worm.getHeight() / 2);
            worm.setVisible(false);
            hole.setWorm(worm);
            punchLevel.mainStage.addActor(worm);
        }
    }

    public void unlockHole() {
        boolean newEnabledHole = false;
        while (!newEnabledHole) {
            //Get random existing hole position
            int randHiddenHole = MathUtils.random(0, punchLevel.holes.size() - 1);
            if (!punchLevel.holes.get(randHiddenHole).isEnabled()) {
                punchLevel.holes.get(randHiddenHole).setEnabled(true);
                punchLevel.holes.get(randHiddenHole).setCreated(true);
                punchLevel.holesCreated++;
                newEnabledHole = true;
            }
        }
    }

    public void generateWorm() {
        {
            //Select random hole and generate the worm
            final int randHole = MathUtils.random(0, punchLevel.holes.size() - 1);
            if (punchLevel.holes.get(randHole).isEnabled()) {
                //Reset spawnTimer
                if (punchLevel.spawnTimer > punchLevel.spawnInterval) {
                    punchLevel.spawnTimer -= punchLevel.spawnInterval;
                }
                punchLevel.holes.get(randHole).setVisible(true);
                punchLevel.holes.get(randHole).setEnabled(false);

                final Worm worm = punchLevel.holes.get(randHole).getWorm();

                //TODO update with new worms
                //Set random type depending on higher score
                //final int randType = punchLevel.score / 7 > 2 ? MathUtils.random(1, punchLevel.score / 7) : 2;
                final int randType = MathUtils.random(1, 30);
                if (randType < 25) {
                    worm.setWormType(0);
                } else if (randType >= 25) {
                    worm.setWormType(1);
                }
                worm.setVisible(true);
                worm.setActiveAnimation("grow"+worm.wormType);
                worm.inputListener = new InputListener() {
                    public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                        if (!punchLevel.isPaused()) {
                            worm.punched++;
                            switch (worm.wormType) {
                                case 0:
                                    worm.setActiveAnimation("crush"+worm.wormType);
                                    punchLevel.score++;
                                    worm.removeListener(this);
                                    break;
                                case 1:
                                    if (worm.punched < worm.getPunchLimit()) {
                                        worm.setActiveAnimation("crush"+worm.wormType);
                                    } else {
                                        worm.setActiveAnimation("boom"+worm.wormType);
                                        punchLevel.score+= 3;
                                        worm.removeListener(this);
                                    }
                                    break;
                            }
                            punchLevel.beep.play(punchLevel.volume);
                            Gdx.input.vibrate(50);
                        }
                        return true;
                    }
                };
                worm.addListener(worm.inputListener);
            }
        }
    }

    public void changeWormAnimations() {
        for (Hole hole : punchLevel.holes) {
            Worm worm = hole.getWorm();
            if(worm.getElapsedTime() > worm.getActiveAnim().getAnimationDuration() && worm.isVisible()) {
                if (worm.getActiveName().equals("grow" + worm.wormType)) {
                    worm.setActiveAnimation("stand" + worm.wormType);
                }else if (worm.getActiveName().equals("stand" + worm.wormType) && worm.getElapsedTime() > worm.getStandDuration()) {
                    worm.setActiveAnimation("escape" + worm.wormType);
                }else if (worm.getActiveName().equals("crush" + worm.wormType)) {
                    if (worm.punched >= worm.getPunchLimit()) {
                        worm.setVisible(false);
                        worm.removeListener(worm.inputListener);

                        hole.setEnabled(true);
                    } else {
                        worm.setActiveAnimation("stand" + worm.wormType);
                    }
                }else if (worm.getActiveName().equals("escape" + worm.wormType)) {
                    worm.setVisible(false);
                    worm.setActiveName("");
                    worm.removeListener(worm.inputListener);

                    hole.setEnabled(true);

                    punchLevel.escaped++;
                    punchLevel.lives--;
                }else if (worm.getActiveName().equals("boom" + worm.wormType)) {
                    worm.setVisible(false);
                    worm.setActiveName("");
                    worm.removeListener(worm.inputListener);

                    hole.setEnabled(true);
                }
            }
        }
    }
}
