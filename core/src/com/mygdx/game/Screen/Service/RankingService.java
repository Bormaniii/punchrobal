package com.mygdx.game.Screen.Service;

import com.badlogic.gdx.files.FileHandle;

import java.util.ArrayList;
import java.util.Collections;

public class RankingService {
    FileHandle rankFile;

    public RankingService(FileHandle rankFile) {
        this.rankFile = rankFile;
    }

    public String[] readStringFile() {
        return rankFile.readString().split(" ");
    }

    private int[] getScoresAsIntegers() {
        String[] scores = readStringFile();
        int[] scoresInt = new int[scores.length];
        for (int i = 0; i < scoresInt.length; i++) {
            scoresInt[i] = Integer.parseInt(scores[i]);
        }
        return scoresInt;
    }

    public ArrayList<Integer> getSortedScores(int newScore) {
        int[] scoresInt = getScoresAsIntegers();

        //Add to new ArrayList and sort
        ArrayList<Integer> scoresArray = new ArrayList();
        for (int i = 0; i < scoresInt.length; i++) {
            scoresArray.add(scoresInt[i]);
        }
        scoresArray.add(newScore);
        Collections.sort(scoresArray, Collections.<Integer>reverseOrder());
        return scoresArray;
    }

    public void clearFile() {
        rankFile.write(false);
    }

    public void writeNewScore(String newScore) {
        rankFile.writeString(newScore + " ", true);
    }
}

